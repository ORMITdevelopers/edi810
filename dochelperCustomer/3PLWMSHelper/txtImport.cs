﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDI810
{
    internal class txtImport
    {
        private static string ediExportPath = EDI810.Properties.Settings.Default.ediImportPath;
        private static string importFileName = EDI810.Properties.Settings.Default.importFileName;

        public Array formatArrary() { 
            string[] lines = System.IO.File.ReadAllLines(ediExportPath + "\\" + importFileName);
            //String formatstring = string.Join(",", lines);
            return lines;
        }

    }
}
