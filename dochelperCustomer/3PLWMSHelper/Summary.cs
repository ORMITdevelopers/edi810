﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDI810
{
    class Summary
    {
        //private static string sqlString_S = System.Configuration.ConfigurationManager.AppSettings["sqlQuery_S"];

        //private static string sqlString_S_1 = "SELECT 'S' AS Summary,T0.DOCTOTAL-T0.VATSUM AS AINVAMTEXGST, T0.VATSUM, T0.DOCTOTAL, T0.DOCENTRY FROM dbo.OINV(NOLOCK) T0 LEFT OUTER JOIN dbo.OCRD(NOLOCK) T3 ON T0.CARDCODE=T3.CardCode  WHERE " +
        //    // "--T3.groupcode=113  AND ISNULL(T0.U_MCS_InvSent,'')=''" +
        //    " t0.docnum in(";
        //private static string sqlString_S_2 = ") order by DOCENTRY";

        public DataTable GetSummaryData(SqlConnection conn, String value1)
        {
            DataTable dt_s = new DataTable();
            var sqlStr = String.Format( "SELECT 'S' AS Summary,T0.DOCTOTAL-T0.VATSUM AS AINVAMTEXGST, T0.VATSUM, T0.DOCTOTAL, T0.DOCENTRY FROM dbo.OINV(NOLOCK) T0 LEFT OUTER JOIN dbo.OCRD(NOLOCK) T3 ON T0.CARDCODE=T3.CardCode  WHERE " +
                " t0.docnum in({0}) " +
                " union all " +
                "SELECT 'S' AS Summary, T0.DOCTOTAL - T0.VATSUM AS AINVAMTEXGST, T0.VATSUM, T0.DOCTOTAL*-1, T0.DOCENTRY FROM dbo.ORIN(NOLOCK) T0 LEFT OUTER JOIN dbo.OCRD(NOLOCK) T3 ON T0.CARDCODE = T3.CardCode  WHERE " +
                " t0.docnum in({0}) " +
                " order by DOCENTRY",value1);
            SqlCommand cmd = new SqlCommand(sqlStr, conn);
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
                dt_s.Load(sdr);
            }

            return dt_s;

        }

        public String SummaryProcesses(DataRow summaryDT, long interchangeControlNum, long controlNum, String qtySum, String numOfItems, String docEntry, String linSum, String lastUnit) 
        {
            //TDS
            String amount_s = summaryDT["DOCTOTAL"].ToString();
            double amount = Math.Round(Convert.ToDouble(amount_s),2);
            int amountInt = Convert.ToInt32(amount * 100);
            String TDS = "TDS*" + amountInt + "";

            //SAC
            String SAC = "";
            String vatSum_s = summaryDT["VATSUM"].ToString();
            int vatSum = (int)(Math.Round(Convert.ToDouble(vatSum_s), 2) * 100);
            if (vatSum_s == "0.000000")
            {
                SAC = "SAC*N*D360*************SUMMARY HST" + "";
            }
            else
            {
                SAC = "SAC*N*D360***" + vatSum + "**********SUMMARY HST" + "";
            }
            
            
            //ISS
            String unit = lastUnit;//ca or ea or xxx
            String ISS = "ISS*" + qtySum + "*" + unit + "";

            //CTT
            String CTT = "CTT*" + numOfItems + "";

            //SE
            int numOfSeg = 6 + 5 + int.Parse(linSum);
            String SE = "SE*" + numOfSeg  + "*" + (controlNum + 1) + "";

            //GE
            String GE = "GE*1*" + controlNum + "";

            //IEA
            String IEA = "IEA*1*" + interchangeControlNum + "";


            String total = TDS + "\r\n" + SAC + "\r\n" + ISS + "\r\n" + CTT + "\r\n" + SE + "\r\n" + GE + "\r\n" + IEA;
            return total; 
        }

    }
}

