﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDI810
{
    class EDIRunning
    {
/*        private static string sqlString1 = System.Configuration.ConfigurationManager.AppSettings["sqlQuery_N_select"];
        private static string sqlString2 = System.Configuration.ConfigurationManager.AppSettings["sqlQuery_N_insert"];
        private static string sqlString3 = System.Configuration.ConfigurationManager.AppSettings["sqlQuery_N_count"];*/

        private static string sqlString1 = "SELECT TOP(1000) [ID]       ,[Customer]       ,[FileName]       ,[DocNum]       ,[InterchangeControlNum] FROM[DocumentHelper].[dbo].[EDIRunning] WHERE ID = @ID";
        private static string sqlString2 = "INSERT INTO [dbo].[EDIRunning]([Customer],[FileName],[DocNum],[InterchangeControlNum]) VALUES(@Customer,@FileName,@DocNum,@InterchangeControlNum)";
        private static string sqlString3 = "SELECT ISNULL(Max([ID]),0)+1       FROM [DocumentHelper].[dbo].[EDIRunning]";



        public DataTable getEDIRecord(SqlConnection conn1) 
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(sqlString1, conn1);
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
                dt.Load(sdr);
            }
            return dt;
        }

        public void insertEDIRecord(SqlConnection conn1, String Customer, String FileName, String DocNum, long InterchangeControlNum)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand(sqlString2, conn1);
                cmd.Parameters.AddWithValue("@Customer", Customer);
                cmd.Parameters.AddWithValue("@FileName", FileName);
                cmd.Parameters.AddWithValue("@DocNum", DocNum);
                cmd.Parameters.AddWithValue("@InterchangeControlNum", InterchangeControlNum);
                //cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: ", ex);
            }

        }

        public String getRowNum(SqlConnection conn1)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(sqlString3, conn1);
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
                dt.Load(sdr);
            }
            String rowNum = dt.Rows[0][0].ToString();
            return rowNum;
        }
    }
}
