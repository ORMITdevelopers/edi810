﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDI810
{
    class Header {
        //private static string sqlString_H = System.Configuration.ConfigurationManager.AppSettings["sqlQuery_H"];

        

        public DataTable GetHeaderData(SqlConnection conn, String value1) { 
            DataTable dt_h = new DataTable();
            var sqlStr=String.Format("SELECT 'H' AS Header,    T0.DocNum,                CAST(T0.DocNum AS NVARCHAR(100)) as 'InvDocNum',                    T0.U_MCS_ASNnumber as 'ASNno',                    T0.U_MCS_CompGLN,                    T0.U_MCS_PartnerGLN,                    T1.DocNum,        t3.[U_OM_EDI_BTCODE],        t3.[U_OM_EDI_BTNAME],        t3.[U_OM_EDI_STNAME],        t3.[U_OM_EDI_STCODE],                    T1.DocDate,                    '388' as Field_1,                   Convert(CHAR(8),T0.DocDate,112) as 'InvDocDate' ,                    '9' as Field_2,                    T0.NumAtCard,                    T0.DocCur,                    '0000' as Field_3,                    T0.TrackNo,                    T0.U_MCS_SupplierID,                    '34007368925' as Field_4,                    T0.U_MCS_ShipToCode,                    T0.ShipToCode,                    T0.U_MCS_INVSent,                    T0.DocEntry,                    T3.LicTradNum,                        '' AS U_MCS_RemitToID,                    CAST(T0.U_MCS_PartnerGLN as varchar)+'_INV_'+CAST(T0.DocNum as varchar)+'.txt' AS FileName FROM dbo.OINV(NOLOCK) T0 LEFT OUTER JOIN dbo.ODLN(NOLOCK) T1 ON T1.DOCNUM=(SELECT TOP 1 T2.BASEREF FROM dbo.INV1 T2 WHERE T2.DOCENTRY=T0.DOCENTRY) LEFT OUTER JOIN dbo.OCRD(NOLOCK) T3 ON T0.CARDCODE=T3.CardCode WHERE  t0.docnum in " +
                "({0}) " +
                " union all " +
                "SELECT 'H' AS Header,T0.DocNum, T0.NumAtCard as 'InvDocNum',                    T0.U_MCS_ASNnumber as 'ASNno',                    T0.U_MCS_CompGLN,                    T0.U_MCS_PartnerGLN,                    T1.DocNum,        t3.[U_OM_EDI_BTCODE],        t3.[U_OM_EDI_BTNAME],        t3.[U_OM_EDI_STNAME],        t3.[U_OM_EDI_STCODE],                    T1.DocDate,                    '388' as Field_1,                   Convert(CHAR(8), T0.DocDate, 112) as 'InvDocDate' ,                    '9' as Field_2,                    T0.NumAtCard,                    T0.DocCur,                    '0000' as Field_3,                    T0.TrackNo,                    T0.U_MCS_SupplierID,                    '34007368925' as Field_4,                    T0.U_MCS_ShipToCode,                    T0.ShipToCode,                    T0.U_MCS_INVSent,                    T0.DocEntry,                    T3.LicTradNum,                        '' AS U_MCS_RemitToID, CAST(T0.U_MCS_PartnerGLN as varchar)+'_INV_' + CAST(T0.DocNum as varchar) + '.txt' AS FileName FROM dbo.ORIN(NOLOCK) T0 LEFT OUTER JOIN dbo.ODLN(NOLOCK) T1 ON T1.DOCNUM = (SELECT TOP 1 T2.BASEREF FROM dbo.RIN1 T2 WHERE T2.DOCENTRY = T0.DOCENTRY) LEFT OUTER JOIN dbo.OCRD(NOLOCK) T3 ON T0.CARDCODE = T3.CardCode WHERE T0.U_MCS_ReasonCode IN('DMG','RTN') AND t0.docnum in " +
                "({0}) " +
                "order by DocEntry",value1);


            SqlCommand cmd = new SqlCommand(sqlStr, conn);
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
                dt_h.Load(sdr);
            }

            return dt_h; 
        
        }

        public DataTable HeaderProcesses(DataRow row, long controlNum, long interchangeControlNum) {
            String docEntry = row["DocEntry"].ToString();
            string docNum = row["DocNum"].ToString();
            //ISA
            DateTime localdate = DateTime.Now;
            String date = localdate.ToString("yyMMdd");
            String time = localdate.ToString("HHmm");
            String senderId = "12";//ISA Qualifier & ID: 12/4162889298
            String senderIdentifier = "4162889298";//GS ID, hard code and get from 
            //String interchangeControlNumber = docEntry;//autoincreace
            String ISA = "ISA*00*          *00*          *" + senderId +"*"+ senderIdentifier + "     *ZZ*METROAS2       *" + date + "*" + time + "*X*00401*" + interchangeControlNum + "*0*P*>";//P for live, T for test
            
            //GS
            String networkId = "5146431000";//
            String date1 = localdate.ToString("yyyyMMdd");
            //String controlNumber = docEntry;//autoincreace
            String GS = "GS*IN*" + senderIdentifier + "*" + networkId + "*" + date1 + "*" + time + "*" + controlNum + "*X*004010VICS" + "";
            
            //ST
            //String docEntry = row["DocEntry"].ToString();
            String ST = "ST*810*" + (controlNum + 1) + "";
            
            //BIG
            String invDocDate = row["InvDocDate"].ToString();   //.Substring(0,10);
        //   DateTime invDocD = DateTime.ParseExact(invDocDate_r, "yyyyMMdd", CultureInfo.InvariantCulture);
        //    String invDocDate = invDocDate_r.Substring(6,4) + invDocDate_r.Substring(3,2) + invDocDate_r.Substring(0,2);
            String invDocNum = row["InvDocNum"].ToString();
            String BIG = "BIG*" + invDocDate + "*" + invDocNum + "";
            
            //REF
            String REF = "REF*DP*4260" + "";

            //N1N4
            String storeName_BT = row["U_OM_EDI_BTNAME"].ToString();
            String partyCode_BT = row["U_OM_EDI_BTCODE"].ToString();
            String storeName_ST = row["U_OM_EDI_STNAME"].ToString();
            String partyCode_ST = row["U_OM_EDI_STCODE"].ToString();

            //String storeName_BT = "";
            //String partyCode_BT = "";
            //String storeName_ST = "";
            //String partyCode_ST = "";

            String N1 = "N1*BT*" + storeName_BT + "*9*" + partyCode_BT;//9: D-U-N-S+4, D-U-N-S Number with Four Character Suffix
            N1 = N1 + "\r\n" + "N1*VN*YOUNG & YOUNG TRADING CO. LTD*9*2017881060000" + "";//DUNS: 2017881060000
            N1 = N1 + "\r\n" + "N1*ST*" + storeName_ST + "*91*" + partyCode_ST + "";//91:Assigned by Seller or Seller's Agent

            //PER
            //ITD
            //DTM
            //FOB


            String total = ISA + "\r\n" + GS + "\r\n" + ST  + "\r\n" + BIG + "\r\n" + REF + "\r\n" + N1;

            DataTable headerResult = new DataTable();
            DataRow row_header = headerResult.NewRow();
            headerResult.Columns.Add("String");
            row_header["String"] = total;
            headerResult.Columns.Add("invDocNum");
            row_header["invDocNum"] = invDocNum;
            headerResult.Columns.Add("storeName");
            row_header["storeName"] = storeName_ST;
            headerResult.Columns.Add("DocNum");
            row_header["DocNum"] = docNum;
            headerResult.Rows.Add(row_header);



            return headerResult;
        }

    }
}
