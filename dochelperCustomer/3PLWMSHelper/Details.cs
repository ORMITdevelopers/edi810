﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDI810
{
    class Details
    {
        //private static string sqlString_D = System.Configuration.ConfigurationManager.AppSettings["sqlQuery_D"];
        //private string sqlString_D_1 = "SELECT 'D' AS Detail,                    T0.DOCENTRY,                    ROW_NUMBER() OVER (ORDER BY LINENUM) as LINENUM,                    coalesce(inv1bar.bcdcode,outbar.bcdcode,innerbar.bcdcode,eabar.bcdcode) codebars,        OITM.U_OM_ItemCat5,                    T0.DSCRIPTION,                    T0.ITEMCODE,                    T0.U_MCS_CustCode,                    CAST(T0.QUANTITY AS int) as QUANTITY,                    T0.U_MCS_CustCostQty,                    T0.U_MCS_CustPacks,                    T0.UNITMSR,        CASE T0.UNITMSR WHEN 'Outer Carton' then 'CA'        WHEN 'Each' then 'EA'        else 'EA' end packtype,                    CAST(T0.PriceAfVAT as DECIMAL(18,6)) as PriceAfVAT,                    T0.VATSUM,        T0.VatPrcnt,                    CAST(T0.Price as DECIMAL(18,6)) as Price,                    CAST((T0.LineTotal) as DECIMAL(18,6)) as LINEAMTEXGST,                    CAST((T0.LineTotal+T0.VatSum) as DECIMAL(18,6)) as LINEAMTINCGST                     FROM dbo.INV1 T0 inner join dbo.OINV T1 on T0.DocEntry = T1.DocEntry inner join oitm on oitm.ITEMCODE = t0.ITEMCODE LEFT OUTER JOIN dbo.OCRD(NOLOCK) T3 ON T1.CARDCODE=T3.CardCode left join obcd outbar on outbar.itemcode=oitm.itemcode and outbar.UomEntry=3 left join obcd innerbar on innerbar.itemcode= oitm.itemcode and innerbar.UomEntry=2 left join obcd eabar on eabar.ItemCode= oitm.ItemCode and eabar.UomEntry=1 left join obcd inv1bar on inv1bar.ItemCode= oitm.ItemCode and inv1bar.UomEntry= t0.UomEntry WHERE " +
        //    //"t1.docdate> DATEADD(d,-10, [dbo].[GetCompanyTime] ()) AND T3.groupcode=113 " +
        //    " t1.docnum in(";
        //private string sqlString_D_2 = ") order by DOCENTRY , t0.LineNum";


        public DataTable GetDetailsData(SqlConnection conn, String value1)
        {
            DataTable dt_d = new DataTable();
            var sqlStr = String.Format( "SELECT 'D' AS Detail,                    T0.DOCENTRY,                    LINENUM as LINENUM,                    coalesce(inv1bar.bcdcode,outbar.bcdcode,innerbar.bcdcode,eabar.bcdcode) codebars,        OITM.U_OM_ItemCat5,                    T0.DSCRIPTION,                    T0.ITEMCODE,                    T0.U_MCS_CustCode,                    CAST(T0.QUANTITY AS int) as QUANTITY,                    T0.U_MCS_CustCostQty,                    T0.U_MCS_CustPacks,                    T0.UNITMSR,        CASE T0.UNITMSR WHEN 'Outer Carton' then 'CA'        WHEN 'Each' then 'EA'        else 'EA' end packtype,                    CAST(T0.PriceAfVAT as DECIMAL(18,6)) as PriceAfVAT,                    T0.VATSUM,        T0.VatPrcnt,                    CAST(T0.Price as DECIMAL(18,6)) as Price,                    CAST((T0.LineTotal) as DECIMAL(18,6)) as LINEAMTEXGST,                    CAST((T0.LineTotal+T0.VatSum) as DECIMAL(18,6)) as LINEAMTINCGST                     FROM dbo.INV1 T0 inner join dbo.OINV T1 on T0.DocEntry = T1.DocEntry inner join oitm on oitm.ITEMCODE = t0.ITEMCODE LEFT OUTER JOIN dbo.OCRD(NOLOCK) T3 ON T1.CARDCODE=T3.CardCode left join obcd outbar on outbar.itemcode=oitm.itemcode and outbar.UomEntry=3 left join obcd innerbar on innerbar.itemcode= oitm.itemcode and innerbar.UomEntry=2 left join obcd eabar on eabar.ItemCode= oitm.ItemCode and eabar.UomEntry=1 left join obcd inv1bar on inv1bar.ItemCode= oitm.ItemCode and inv1bar.UomEntry= t0.UomEntry WHERE  " +
                "t1.docnum in({0}) " +
                " union all " +
                "SELECT 'D' AS Detail, T0.DOCENTRY,                    LINENUM as LINENUM,                    coalesce(RIN1bar.bcdcode, outbar.bcdcode, innerbar.bcdcode, eabar.bcdcode) codebars,        OITM.U_OM_ItemCat5,                    T0.DSCRIPTION,                    T0.ITEMCODE,                    T0.U_MCS_CustCode,                    CAST(T0.QUANTITY AS int)*-1 as QUANTITY,                    T0.U_MCS_CustCostQty,                    T0.U_MCS_CustPacks,                    T0.UNITMSR,        CASE T0.UNITMSR WHEN 'Outer Carton' then 'CA'        WHEN 'Each' then 'EA'        else 'EA' end packtype, CAST(T0.PriceAfVAT as DECIMAL(18, 6)) as PriceAfVAT,                    T0.VATSUM,        T0.VatPrcnt,                    CAST(T0.Price as DECIMAL(18, 6)) as Price,                    CAST((T0.LineTotal) as DECIMAL(18, 6)) as LINEAMTEXGST,                    CAST((T0.LineTotal + T0.VatSum) as DECIMAL(18, 6)) as LINEAMTINCGST                     FROM dbo.RIN1 T0 inner join dbo.ORIN T1 on T0.DocEntry = T1.DocEntry inner join oitm on oitm.ITEMCODE = t0.ITEMCODE LEFT OUTER JOIN dbo.OCRD(NOLOCK) T3 ON T1.CARDCODE = T3.CardCode left join obcd outbar on outbar.itemcode = oitm.itemcode and outbar.UomEntry = 3 left join obcd innerbar on innerbar.itemcode = oitm.itemcode and innerbar.UomEntry = 2 left join obcd eabar on eabar.ItemCode = oitm.ItemCode and eabar.UomEntry = 1 left join obcd RIN1bar on RIN1bar.ItemCode = oitm.ItemCode and RIN1bar.UomEntry = t0.UomEntry WHERE " +
                "t1.docnum in({0}) " +
                "order by DOCENTRY , LineNum",value1);
            SqlCommand cmd = new SqlCommand(sqlStr, conn);
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
                dt_d.Load(sdr);
            }

            return dt_d;

        }
        public DataTable DetailsProcesses(DataTable detailsDT)
        {
            String total = "";
            int numOfItems = 0;
            int qtySum = 0;
            int lineSum = 0;
            String lastUnit = "";
            for (int i = 0; i < detailsDT.Rows.Count; i++)
            {
                //IT1
                String qty = detailsDT.Rows[i]["QUANTITY"].ToString();
                int qty_int = (int)detailsDT.Rows[i]["QUANTITY"];
                String unit = "";
                String UPCType = "";//UK-> UP->

                String codeBar = "";
                codeBar = detailsDT.Rows[i]["CODEBARS"].ToString();
                if (codeBar.Equals(""))
                {
                    codeBar = detailsDT.Rows[i]["U_OM_ItemCat5"].ToString();
                }
                if (detailsDT.Rows[i]["packtype"].ToString() == "EA")
                {
                    unit = "EA";
                    if (codeBar.Length == 13)
                    {
                        UPCType = "EN";
                    }
                    else
                    {
                        UPCType = "UP";
                    }
                    //UPCType = "UP";
                }
                else if (detailsDT.Rows[i]["packtype"].ToString() == "CA")
                {
                    unit = "CA";
                    UPCType = "UK";
                }
                else if (detailsDT.Rows[i]["packtype"].ToString() == "WRONGPACK")
                {
                    unit = "WRONGUNIT";
                    UPCType = "WRONGPACK";
                }
                String unitPrice_s = detailsDT.Rows[i]["Price"].ToString();
                double unitPrice = Math.Round(Convert.ToDouble(unitPrice_s), 2);

                String IT1 = "IT1**" + qty + "*" + unit + "*" + unitPrice + "**" + UPCType + "*" + codeBar + "";
                
                //PID
                String description = detailsDT.Rows[i]["DSCRIPTION"].ToString();
                String PID = "PID*F****" + description + "";
               
                //SAC
                String vatSum_s = detailsDT.Rows[i]["VATSUM"].ToString();
                int vatSum = (int)(Math.Round(Convert.ToDouble(vatSum_s),2)*100);
                String SAC = "";
                String CPQ = "Z";//charge percent qualifier
                Double percent = Math.Round(Convert.ToDouble(detailsDT.Rows[i]["VatPrcnt"].ToString()),2);
                double rate = unitPrice;
                if (!detailsDT.Rows[i]["VATSUM"].ToString().Equals("0.000000") && !detailsDT.Rows[i]["VatPrcnt"].ToString().Equals("0.000000"))
                {
                    SAC = "SAC*C*D360***" + vatSum + "*" + CPQ + "*" + percent + "*" + rate + "*DO*" + qty + "**06" + "";
                    lineSum = lineSum + 1;
                }


                if (i + 1 == detailsDT.Rows.Count)
                {
                    if (SAC.Equals(""))
                    {
                        total = total + IT1 + "\r\n" + PID;
                    }
                    else
                    {
                        total = total + IT1 + "\r\n" + PID + "\r\n" + SAC;
                    }                    
                }
                else
                {
                    if (SAC.Equals(""))
                    {
                        total = total + IT1 + "\r\n" + PID + "\r\n";
                    }
                    else 
                    {
                        total = total + IT1 + "\r\n" + PID + "\r\n" + SAC + "\r\n";
                    }
                    
                }

                numOfItems = i+1;
                qtySum = qtySum + qty_int;
                lineSum = lineSum + 2;
                lastUnit = unit;
            }


            DataTable detailsResult = new DataTable();
            DataRow row_header = detailsResult.NewRow();
            detailsResult.Columns.Add("String");
            row_header["String"] = total;
            detailsResult.Columns.Add("qtySum");
            row_header["qtySum"] = qtySum;
            detailsResult.Columns.Add("numOfItems");
            row_header["numOfItems"] = numOfItems;
            detailsResult.Columns.Add("lineSum");
            row_header["lineSum"] = lineSum;
            detailsResult.Columns.Add("lastUnit");
            row_header["lastUnit"] = lastUnit;
            detailsResult.Rows.Add(row_header);



            return detailsResult;
        }

    }

}

