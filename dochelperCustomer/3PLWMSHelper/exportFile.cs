﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDI810
{
    class exportFile
    {
        public void fileExport(SqlConnection conn, SqlConnection conn1, String value1, long fix_value1, string ediExportPath) { 
        
            try
            {
                conn1.Open();


                Header header = new Header();
                DataTable headerDT = new DataTable();
                Details details = new Details();
                DataTable detailsDT = new DataTable();
                DataTable detailsDTSource = new DataTable();
                Summary summary = new Summary();
                DataTable summaryDTSource = new DataTable();
                DataTable summaryDT = new DataTable();

                EDIRunning ediRunning = new EDIRunning();
                DataTable ediRecord = new DataTable();
                exportFile export = new exportFile();

                headerDT = header.GetHeaderData(conn, value1.ToString());
                detailsDTSource = details.GetDetailsData(conn, value1.ToString());
                summaryDTSource = summary.GetSummaryData(conn, value1.ToString());


                for (int i = 0; i < headerDT.Rows.Count; i++)
                {
                    int controlNum = Convert.ToInt32(ediRunning.getRowNum(conn1));
                    long interchangeControlNum = controlNum + fix_value1;

                    DataRow row = headerDT.Rows[i];
                    String docEntry = row["DocEntry"].ToString();

                    DataRow headerResultSource = header.HeaderProcesses(row, controlNum, interchangeControlNum).Rows[0];
                    String headerString = headerResultSource["String"].ToString();

                    detailsDT = detailsDTSource.Clone();
                    for (int a = 0; a < detailsDTSource.Rows.Count; a++)
                    {
                        DataRow rowDetails = detailsDTSource.Rows[a];
                        if (rowDetails["DOCENTRY"].ToString().Equals(docEntry))
                        {
                            detailsDT.ImportRow(rowDetails);
                        }
                    }

                    DataRow detailsResultSource = details.DetailsProcesses(detailsDT).Rows[0];
                    String detailsString = detailsResultSource["String"].ToString();

                    //gather info for summary
                    String qtySum = detailsResultSource["qtySum"].ToString();
                    String numOfItems = detailsResultSource["numOfItems"].ToString();
                    String lineSum = detailsResultSource["lineSum"].ToString();
                    String lastUnit = detailsResultSource["lastUnit"].ToString();
                    String storeName = headerResultSource["storeName"].ToString();
                    String docNum = headerResultSource["DocNum"].ToString();

                    String summaryString = "";
                    for (int b = 0; b < summaryDTSource.Rows.Count; b++)
                    {
                        DataRow summaryDetails = summaryDTSource.Rows[b];
                        if (summaryDetails["DOCENTRY"].ToString() == docEntry)
                        {
                            //summaryDT.Rows.Add(summaryDetails);
                            summaryString = summary.SummaryProcesses(summaryDetails, interchangeControlNum, controlNum, qtySum, numOfItems, docEntry, lineSum, lastUnit);
                        }
                    }

                    String EDIResult = headerString + "\r\n" + detailsString + "\r\n" + summaryString;

                    String fileName = "INV810_" + DateTime.Now.ToString("yyyyMMdd") + "-" + docNum;
                    using (StreamWriter writetext = new StreamWriter(Path.Combine(ediExportPath, fileName + ".edi")))
                    {
                        writetext.WriteLine(EDIResult);
                    }


                    ediRunning.insertEDIRecord(conn1, storeName, fileName, docNum, interchangeControlNum);

                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            finally
            {
                conn1.Close();
            }

        }
    }
}
